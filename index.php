<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link
            rel="stylesheet"
            href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
            integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
            crossorigin="anonymous">
        <script
            src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
        <script
            src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous"></script>
        <title>Meg 91</title>
    </head>
    <body>
    <h1>Meg 91 – Mit PHP einen Newsroom erstellen</h1>
    <div class="jumbotron">
        <h1>1: Einen String mit echo ausgeben</h1>
<?php
/*_____________ ERSTE STRING-AUSGABE __________________________________________
AUFGABE:        Legen Sie die Datei index.php an und geben Sie Ihren ersten String aus*/
echo "Hallo Welt";

/*_____________ PHP IN HTML EINBINDEN __________________________________________
AUFGABE:        1. Legen Sie in index.php ein HTML-Grundgerüst an und erstellen Sie im body eine PHP-Sequenz
                2. Geben Sie die PHP-Sequenz einmal innerhalb eines h1-Elements aus
                3. Geben Sie das h1-Element innerhalb der PHP-Sequenz aus
                4. Nutzen Sie für die Ausgabe die verkürzte Schreibweise mit <?=
LINKS:          https://www.php.net/manual/de/language.basic-syntax.phpmode.php
                https://www.php.net/manual/de/language.basic-syntax.instruction-separation.php*/?>
<h2><?php echo "Hallo Welt";?></h2>
<?php echo "<h1>Hallo Welt</h1>";?>
<h2><?= "Hallo Welt";?></h2>
    </div>
<div class="jumbotron">
    <h1>2: Variablen definieren</h1>
<?php
/*_____________ KOMMENTARE ___________________________________________________
AUFGABE:        1. Schreiben Sie einen Kommentare über die jweilige Anweisung, die Sie soeben geschrieben haben.
                   Benutzen Sie dafür beide Schreibweisen.
LINK:           https://www.php.net/manual/de/language.basic-syntax.comments.php

<?php
/*_____________ ERSTE VARIABLE DEFINIEREN ______________________________________
AUFGABE:        Definieren Sie Ihre erste Variable mit dem Namen Newsroom und weisen ihr den
                Stringwert "Newsroom der Meg 92" zu.
LINK:           https://www.php.net/manual/de/language.variables.basics.php*/
$newsroom = "Newsroom der Meg 91";

/*_____________ AUSGABE DER VARIABLE UND EINBETTEN IN HTML___________________________________________
AUFGABE:        Geben Sie diese VariabE mit Hilfe von echo in einem HTML-Element h1 aus
LINK:           https://www.php.net/manual/de/language.basic-syntax.phpmode.php*/?>
<h2><?= $newsroom;?></h2>
</div>
<div class="jumbotron">
    <h1>3: Fehlermeldungen nutzen</h1>
<?php
/*_____________ FEHLERMELDUNGEN NUTZEN___________________________________________
AUFGABE:        1. Ändern Sie den Variablennamen in $nachrichtenraum.
                2. Rufen Sie die Seite erneut im Browser auf.
                3. Werten Sie die angezeigte Fehlermeldung aus.*/?>
<h2><?//= $nachrichtenraum;?></h2>
</div>
<div class="jumbotron">
    <h1>4: Variablen und Strings kombinieren</h1>
<?php
/*_____________ VARIABLEN UND STRINGS KOMBINIEREN___________________________________________
AUFGABE:        1. Ändern Sie den Variablennamen wiedercin $newsroom.
                2. Fügen Sie in der echo-Anweisung den String "Dies ist der ." und die Variable
                   $newsroom zusammen, so dass ein zusammenhängender Text entsteht.
LINK:           https://www.php.net/manual/de/function.echo.php*/?>
<h2><?= "Dies ist der $newsroom";?></h2>
</div>
<div class="jumbotron">
    <h1>5 a: Mit Datumswerten rechnen (objektorientiert) und mit Kontrollstrukturen überprüfen</h1>
<?php
/*_____________ DATUMSOBJEKTE_______________________________________________
AUFGABE:        Definieren Sie mit Hilfe der DateTime-Funktion das aktuelle Datum mit Uhrzeit als Objekt
LINK:           https://www.php.net/manual/de/datetime.construct.php*/
$daytime = new DateTime();

/*_____________ FORMATIEREN VON DATUMSOBJEKTEN ________________________________
AUFGABE:        1. Formatieren Sie das DateTime-Objekt für die Ausgabe im deutschen Datumsformat
                2. Extrahieren Sie mit Hilfe der format-Funktion den vollständigen Namen des aktuellen Monats
                3. Formatieren die das DateTime-Objekt für Ausgabe der aktuellen Tageszeit
LINK:           https://www.php.net/manual/de/datetime.formats.date.php
                https://www.php.net/manual/de/datetime.formats.time.php*/
$date = $daytime->format('d.m.y');
$month = $daytime->format('F');
$time = $daytime->format('H:m');

/*_____________ KONTROLLSTRUKTUREN: FALLUNTERSCHEIDUNG UND VERGLEICHSOPERATOREN_____________________________________________
AUFGABE:        Überprüfen Sie Sie, ob die Tageszeit vor oder nach High Noon ist.
                Entsprechend fällt dann die Grußformel aus.
                Geben Sie das Ergebnis der Kontrollstruktur in einem .card-Container aus
LINKS:           https://www.php.net/manual/de/control-structures.if.php
                https://www.php.net/manual/de/language.operators.comparison.php*/
if ($time >= "12:00")
    {$greeting = "Guten Morgen";}
    else
    {$greeting = "Guten Tag";} ?>
    <div class="card" style="width: 18rem;">
            <div class="card-body">
                <p class="card-text"><?= "$greeting heute ist der $date, ein wunderschöner
                    $month-Tag und wir haben bereits $time";?></p>
            </div>
        </div>
<?php
/*_____________ WERTE VON DATUMSOBJEKTEN VERGLEICHEN: BERECHNEN DER JAHRESZEIT (OBJEKTORIENTIERT)_____________________________________________________________
AUFGABE:        Stellen Sie fest, in welche Jahreszeit wir uns aktuell befinden. Testen Sie dies, indem Sie der
                Variable $daytime ein beliebiges Datum neu zuweisen. Anschließend kommentieren Sie diese wieder aus, so
                dass das aktuelle Systemdatum wieder genutzt wird. Ordnen Sie in jedem Case einer neuen Variable $season
                den String-Wert mit dem Namen der entsprechenden Jahres zu.
LINK:           https://www.php.net/manual/de/control-structures.switch.php*/
$daytime = new DateTime();
switch ($date) {
    case ($daytime>=new DateTime('2020-03-20') && $daytime <= new DateTime('2020-06-19')) : $season = "Frühling";break;
    case ($daytime>=new DateTime('2020-06-20') && $daytime <= new DateTime('2020-09-21')) : $season = "Sommer";break;
    case ($daytime>=new DateTime('2020-09-22') && $daytime <= new DateTime('2020-12-21')) : $season = "Herbst";break;
    case ($daytime>=new DateTime('2020-12-22') && $daytime <= new DateTime('2020-03-19')) : $season = "Winter";break;
    }
?>
</div>
<div class="jumbotron">
    <h1>5 b: Mit Datumswerten rechnen (prozedural)</h1>
<?php
/*_____________ WERTE VON DATUMSOBJEKTEN VERGLEICHEN: BERECHNEN DER JAHRESZEIT (PROZEDURAL)_____________________________________________________________
AUFGABE:        Stellen Sie fest, in welche Jahreszeit wir uns aktuell befinden. Testen Sie dies, indem Sie der
                Variable $daytime ein beliebiges Datum neu zuweisen. Anschließend kommentieren Sie diese wieder aus, so
                dass das aktuelle Systemdatum wieder genutzt wird. Ordnen Sie in jedem Case einer neuen Variable $season
                den String-Wert mit dem Namen der entsprechenden Jahres zu.
LINK:           https://www.php.net/manual/de/control-structures.switch.php*/
$timestamp = time();
echo "<p><strong>Aktueller Timestamp:</strong> ".$timestamp."</p>";
$datum = date("d.m.Y", $timestamp);
$time = date("h:m", $timestamp);
$christmas = strtotime('24.12.2020');
echo "<p><strong>$greeting: </strong>Heute ist der $datum</p>";
echo "<p><strong>$greeting: </strong>Wir haben bereits $time Uhr</p>";


$christmas = strtotime('24.12.2020');
$christmasAlert = abs($christmas - $timestamp);
$daysUntilChristmas= floor($christmasAlert / 60 /60 / 24);
echo "<p><strong>Tage bis Weihnachten: </strong>$daysUntilChristmas</p>";
switch ($datum) {
    case ($timestamp >= strtotime('2020-03-20') && $timestamp <= strtotime('2020-06-19')) : $season = "Frühling";break;
    case ($timestamp >= strtotime('2020-06-20') && $timestamp <= strtotime('2020-09-21')) : $season = "Sommer";break;
    case ($timestamp >= strtotime('2020-09-22') && $timestamp <=  strtotime('2020-12-21')) : $season = "Herbst";break;
    case ($timestamp >= strtotime('2020-12-22') && $timestamp <=  strtotime('2020-03-19')) : $season = "Winter";break;
}
echo "<p><strong>Aktuelle Jahreszeit: </strong>$season</p>";
?>
</div>
<div class="jumbotron">
    <h1>6: Externe Funktionen erstellen und nutzen</h1>
<?php
/*____________ FUNKTIONEN _____________________________________________________
AUFGABE:        Erstellen Sie in einer externen Datei eine Funktion, die den .card-Container mit
                der Begrüßung mit folgendem Text ausgibt:
                Guten XX, heute ist der XX, ein wunderschöner XX(Monat)-Tag im XX (Jahreszeit).
                Es ist XX Uhr und Zeit für aktuelle Nachrichten.
LINK:           https://www.php.net/manual/de/functions.user-defined.php*/
include ('functions/greeting.php');
greeting ($greeting, $date,  $month, $time,$season);
?>
</div>
<div class="jumbotron">
    <h1>Rechnen mit Datumswerten</h1>
<?php
/*_____________ RECHNEN MIT DATUMSWERTEN ___________________________________________________
AUFGABE:        Berechnen Sie die
                Differenz zwischen dem aktuellen Datum und Weihnachten und geben Sie diese dann
                aus.
LINK:           https://www.php.net/manual/de/datetime.diff.php */
$christmas = new DateTime('2020-12-24');
$interval = $daytime->diff($christmas);
$christmasAlert = $interval->format('Es sind noch %a Tage bis Weihnachten');?>
        <div class="card bg-danger mb-3 text-white" style="width: 18rem;">
            <div class="card-header">
                <?= $christmasAlert;?>
            </div>
        </div>
</div>
<div class="jumbotron">
    <h1>7: Navigation aus Array erzeugen</h1>
<?php
/*______________ ARRAYS AUSLESEN _____________________________________________________________
AUFGABE:        1. Erstellen Sie im Ordner content die Datei categories.php und erstellen Sie darin ein einfaches Array mit den
                   Items Sport, Kultur, Politik,Wirtschaft.
                2. Erstellen Sie im Ordner functions die Datei nav.php und schreiben darin eine Funktion, die die Werte des eben
                   erstellten Arrays ausliest und in eine Bootstrap-Navigation schreibt. Iterieren Sie durch das Array mit foreach
                3. Führen Sie die Funktion in dieser Datei aus.
LINK:           https://www.php.net/manual/de/function.array
                https://www.php.net/manual/de/control-structures.foreach.php
                https://www.php.net/manual/de/function.require-once.php*/
require_once ('functions/nav.php');
nav ();
?>
</div>
<div class="jumbotron">
    <h1>8: Assoziatives Array auslesen</h1>
<?php
/*_____________ ASSOZIATIVE ARRAY AUSLESEN ___________________________________________________
AUFGABE:        1. Erstellen Sie im Ordner content die Datei text.php und erstellen Sie darin ein assoziatives Array mit dem
                   Inhalt aus der Datei articles.txt.
                2. Binden Sie diese Datei in die index.php ein
                3. Iterieren Sie mit foreach durch das Array und geben Sie die Artikel aus.
LINK:           https://www.php.net/manual/de/function.array
                https://www.php.net/manual/de/control-structures.foreach.php*/
include ('content/articles.php');

foreach ($articles as $article) : ?>
        <article>
            <h6><?= $article['category'] ;?></h6>
            <h2><?= $article['headline'] ;?></h2>
            <h4><?= $article['lead'] ;?></h4>
            <p><?= $article['text'] ;?></p>
        </article>
<?php endforeach; ?>
</div>
<?php
/*_____________ ARRAY DURCHSUCHEN UND MIT ARRAY_WALK AUSGEBEN _____________________
AUFGABE:        1. Erstellen Sie im Ordner content die Datei categorized_articles.php.
                2. Erstellen Sie darin ein Array mit dem Inhalt der categorized_articles.txt
                   und strukturieren Sie das Arrray entsprechend
                3. Erstellen Sie im Ordner functions die Datei search_articles.php und definieren Sie darin eine Funktion,
                   die die Values der Keys headline, lead und text ausgibt, wenn die Suchbedingung erfüllt ist.
                   Binden Sie diese Datei und categorized_articles.php hier in index.php ein.
                4. Rufen Sie diese Funktion innerhalb der PHP-Funktion array_walk auf.
                   (array_walk(Name des Arrays, Name der Funktion, Suchbegriff))
LINK:           https://www.php.net/manual/de/function.array-walk.php*/
require_once ('functions/search_articles.php');
include ('content/categorized_articles.php');?>
<div class="jumbotron">
<h1>9: Mehrdimensionales Array mit Suchbegriff filtern</h1>
<?php
$search_tag = 'Sport';
array_walk($categorized_articles,"search_articles",$search_tag);
?>
</div>
    <div class="jumbotron">
        <h1>10: Mit Datenbank verbinden und Ergebnis als Array ausgeben</h1>
<?php
/*______________ MIT DATENBANK VERBINDEN _____________________________________________________
AUFGABE:        1. Erstellen Sie im Ordner inc die Datei dbconnect und erstellen Sie darin
                   eine Verbindung zur MySQL-Datenbank meg 91 mit Hilfe des PHP Data Objects
                   Datenbankname: meg 91, Benutzer: meg91, PW: meg9100
                2. Binden Sie die Datei über require_once in die index.php ein
LINKS:          https://www.php.net/manual/de/book.pdo.php
                https://www.php.net/manual/de/pdo.construct.php*/
require_once ('inc/dbconnect.php');
?>
<?php
/*_____________ WERTE AUS DATENBANK AUSLESEN _________________________________________________
AUFGABE:        1. Erstellen Sie in index.php eine Instanz der Datenbankverbindung aus dbconnect
                2. Schreiben Sie in SQL-Statement, das alle Werte aus der Tabelle tbl_categories ausliest
                   und weisen Sie dieses einer neuen Variable ($query) als String-Wert zu.
                3. Führen Sie das Statement mit prepare und fetchAll (PDO::FETCH_ASSOC)aus.
                4. Geben Sie das Ergebnis mit print_r aus.
                5. "Hübschen Sie die Ausgabe mit <pre> auf */
$query = 'SELECT * FROM tbl_categories';
$rs = $db->prepare($query);
$rs->execute();?>
<pre>
<?php print_r($rs->fetchAll(PDO::FETCH_ASSOC)); ?>
</pre>
    </div>
    <div class="jumbotron">
        <h1>11: Menu aus Datenbank erstellen</h1>
<?php
/*_____________ MENU ERSTELLEN MIT FUNKTION  _______________________________________________
AUFGABE:        1. Duplizieren Sie die Datei nav.php und benennen die neue Datei in navdb.php um und
                   die Funktion  in navdb.
                2. Ersetzen die bisher includierte Datei durch dbconnect.php.
                3. Binden Sie die eben erstellte Abfrage in nav.db.php ein und weisen Sie fetchAll der
                   Variable $categories zu.
                5. Binden Sie navdb.php in index.php ein rufen Sie die Funktion darin auf. */
require_once ('functions/navdb.php');
navdb(); ?>
    </div>
        <div class="jumbotron">
            <h1>12: Artikel aus Datenbank ausgeben und mit foreach-Schleife ausgeben</h1>
<?php
/*_____________ Artikel aus Datenbank abfragen und mit foreach-Schleife ausgeben ____________________
AUFGABE:        1. Wählen Sie nach dem gleich Muster wie beim Menu eben die Artikel aus der Datenbank meg91
                   aus. Legen Sie dann in der Schleife eine weitere Schleife an, um die zum Artikel gehörenden
                   Absätze auszugeben.
                2. Überprüfen Sie dabei, on der Absatz eine eigene Überschrift hat oder nicht.
                3. Da die Text direkt über phpmyadmin erfasst wurden, zeigt der Brwoser bei Umlauten
                   kryptische Zeichen. Eliminieren Sie diese durch den Einsatz der Funktion utf8_encode.
LINK:           https://www.php.net/manual/de/function.utf8-encode */
$query = "SELECT * FROM tbl_articles";
$rs= $db->prepare($query); $rs->execute(); $res=
        $rs->fetchAll(PDO::FETCH_ASSOC);
        foreach ($res as $key => $row) : ?>
        <article>
            <?$id_article = $row['id_article'];?>
            <h2><?= $row['headline'];?></h2>
            <h3><?= $row['lead'];?></h3>
            <p><?= $row['text'];?></p>
            <section>
                <?php $query = "SELECT * FROM tbl_paragraphs WHERE id_p2a =".$id_article; $rs=
                $db->prepare($query);
                $rs->execute();
                $ps= $rs->fetchAll(PDO::FETCH_ASSOC);
                foreach ($ps as $key => $p) : ?>
                    <?php if (isset($p['p_headline'])) : ?>
                    <h4><?=  utf8_encode( $p['p_headline']);?></h4>
                <?php endif;?>
                <p><?=$p['p_text'];?></p>
                <?php endforeach ?>
            </section>
        </article>
        <?php endforeach ?>
        </div>
    <div class="jumbotron">
        <h1>13: Daten per Formular an Datenbank übergeben</h1>
<?php
/*_______________ DATEN PER FOIRMULAR EINGEBEN ___________________________________________
AUFGABE:        1. Legen Sie ein einfaches Formular (method=post)an, mit dem ein neuer Artikel erfasst werden kann.
                   (Felder: headline, lead, pub_status)
                2. Erstellen Sie den Ordner pages und darin die Datei article_input.php. Geben Sie darin den Inhalt
                   der Variable $_POST per print und pre aus und schauen Sie sich die Struktur des Arrays an
                3. Definieren Sie dann Variablen für jedes item aus diesem Array und kommentieren Sie pre und
                   print_r wieder aus.
                4. Erstellen Sie die Variable $query mit einem Input-Statement.
                5. Führen Sie dieses Statement wie gehabt mit prepare und execute aus.
                6. Geben Sie den gespeicherten Artikel auf der gleichen Seite wieder aus
LINK:           https://www.php.net/manual/de/reserved.variables.post */
?>

<form method="POST" action="pages/article_input.php">
  <div class="form-group">
    <label for="headline">Headline</label>
    <input type="text" class="form-control" id="headline" name="headline" aria-describedby="headlineHelp">
    <small id="headlineHelp" class="form-text text-muted">Hier geben Sie die Headline ein</small>
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Lead</label>
    <input type="text" class="form-control" id="lead" name="lead">
  </div>
  <div class="form-group">
  <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="pub_status" id="pub_status1" value="1">
        <label class="form-check-label" for="pub_status1">in Bearbeitung</label>
  </div>
  <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="pub_status" id="pub_status2" value="2">
        <label class="form-check-label" for="pub_status1">zur Genehmigung</label>
  </div>
  <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="pub_status" id="pub_status3" value="3">
        <label class="form-check-label" for="pub_status1">freigegeben</label>
  </div>
  </div>
  <div class="btn-group" role="group" aria-label="Artikel speichern">
        <button type="submit" class="btn btn-primary">Speichern</button>
  </div>
    </form>
    </div>

    </body>
</html>

