<?php
/* Mehrdimensionales Array */
$categorized_articles= [
	'0' => ['Kultur'=> [
            "headline"  => "Kulturartikel 1",
            "lead"      => "Kulturlead 1",
            "author"    => "Autor 1",
            "text"      => "Text 1"
            ],
    ],
    '1' => ['Kultur'    =>[
            "headline"  =>"Kulturartikel 2",
            "lead"      =>"Kulturlead 2",
            "author"    => "Autor 2",
            "text"      => "Text 2"
            ],
        ],
    '2' => ['Kultur'    =>[
            "headline"  =>"Kulturartikel 3",
            "lead"      =>"Kulturlead 3",
            "author"    => "Autor 3",
            "text"      => "Text 3"
            ]
    ],
    '3' => ['Sport'     => [
            "headline"  =>"Sportartikel 4",
            "lead"      =>"Sportlead 4",
            "author"    => "Autor 4",
            "text"      => "Text 4"
        ],
    ],
    ];
