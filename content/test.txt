Er werde als ein amerikanischer Präsident regieren, sagte Biden, nicht als Demokrat. An Trumps Wähler gerichtet, sagte Biden: "Ich verstehe eure Enttäuschung.

Ich habe selbst ein paar Mal verloren, aber lasst uns jetzt gegenseitig eine Chance geben."
Es sei an der Zeit, das Land zu "heilen", die "Ära der Verteufelung" müsse enden. "Wir müssen aufhören, unsere Gegner wie Feinde zu behandeln. Sie sind keine Feinde. Sie sind Amerikaner." Weiter versprach Biden, "ein Präsident zu sein, der nicht spalten, sondern vereinen will".
