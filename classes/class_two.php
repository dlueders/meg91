<?php
class class_two {
    /*Properties definieren*/
    public $name;
    public $age;
    public $weight;

    /* Konstruktor anlegen. Wird bei jedem Klassenaufruf ausgeführt im Gegensatz zu Funktion, die explizit aufgerufen werdfen muss.*/
    function __construct($name, $age, $weight){
        $this->name     = $name;
        $this->age      = $age;
        $this->weight   = $weight;
    }

    /*Funktion definieren, die in xder Instanz explizit aufgerufen werden muss.*/
    function returnProperty(){
        $text =  'Mein Name ist '.$this->name.'. Mein Alter ist '.$this->age.'. Mein Gewicht ist '.$this->weight;
        return $text;
    }
}
