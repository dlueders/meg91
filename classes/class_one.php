<?php
class class_one {
/* Eigenschaften deklarieren */
    public $property_one;
    public $property_two = 'Property Number Two';
    /* Methode definieren für die Rückgabe von Werten
    */
    function getProperty(){
        return $this->property_two;
    }
}

