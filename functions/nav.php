<?php
function nav () {

include ('./content/categories.php'); ?>
    <ul class="nav">
    <?php foreach ($categories as $category) :?>
        <li class="nav-item">
            <a class="nav-link" href="#"><?= $category; ?></a>
        </li>
    <?php endforeach; ?>
    </ul>
<?php }
