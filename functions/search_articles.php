<?php
function search_articles($value,$key, $search)
{
    $article = array($key => $value);
    if (isset($value[$search])) : ?>
        <h2><?= $value[$search]['headline']; ?></h2>
        <h4><?= $value[$search]['lead']; ?></h4>
        <p><?= $value[$search]['author']; ?></p>
        <p><?= $value[$search]['text']; ?></p>
    <?php endif;
}
