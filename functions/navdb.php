<?php
function navdb () {

    include ('./inc/dbconnect.php');
    $query = 'SELECT * FROM tbl_categories';
    $rs = $db->prepare($query);
    $rs->execute();
    $categories = $rs->fetchAll(PDO::FETCH_ASSOC)?>
    <ul class="nav">
    <?php foreach ($categories as $category) :?>
        <li class="nav-item">
            <a class="nav-link" href="#"><?= $category['category']; ?></a>
        </li>
    <?php endforeach; ?>
    </ul>
<?php }
